import numpy as np
import os
import pyedflib
import math
import sklearn
import sklearn.decomposition as skd
import sklearn.discriminant_analysis as skda
import matplotlib.pyplot as plt

print "sklearn version", sklearn.__version__
print "welcome to Neurotone Engine"
settings = {  "numState_unused":3,
            "samplingRate":122, "lenAnal":128,
            "numPC":10, "sigmaCoef": 0.5,
            "diap_start":3,  #hz
            "diap_end":45, #hz,
            "NFFT": 128, # NFFT = nextpow2(L)
            "overlap_sampling":10,
            "sigmaForAtefactNeutral":30,
            "kNear":60
         }
def nextpow2(i):
    n = 1
    while n < i: n *= 2
    return n
def euclidDist(a,b):
    return np.sqrt(np.sum((np.square(a-b))))

def get_spectrum(data, Fs, NFFT):
    L = data.shape[0]
    Y = np.fft.fft(data, NFFT)/L
    f = Fs/2.0 * np.linspace(0, 1, num = int(NFFT/2+1))
    sp = np.abs(np.real(Y[0:int(NFFT / 2.0 + 1)]))
    return sp, f

def calcFeatureSp(pointData, sett):
    spectre_good = []
    for chan in range(0, pointData.shape[0]):
        featureData = pointData[chan,:]
        spectre, freq = get_spectrum(featureData, sett['samplingRate'], sett['NFFT'])
        spectre_good.append(spectre[(freq>=sett["diap_start"])
                                    *(freq<=sett["diap_end"])])
    spectreCh = np.asarray(spectre_good)
    return spectreCh.reshape(-1)

def train(dataEEGpath, sett):
    dataEEGfiles = os.listdir(dataEEGpath)
    numClasses = len(dataEEGfiles)
    k = 0
    Ctrain=[]
    dataTrain = []
    featuresTrain = []
    for state in range(1,numClasses+1):
        f = pyedflib.EdfReader(dataEEGpath + "\\" + str(state)+".edf")
        n = f.signals_in_file
        signal_labels = f.getSignalLabels()
        sigbufs = np.zeros((n, f.getNSamples()[0]))
        for i in np.arange(n):
            sigbufs[i, :] = f.readSignal(i)
        data = sigbufs
        dataMean = np.mean(data,0)
        datarep = np.tile(dataMean, (data.shape[0], 1))
        dataNormed = data - datarep
        length = dataNormed.shape[1]
        t=0
        while (t * np.floor(sett["samplingRate"] / sett["overlap_sampling"]) + sett["lenAnal"]) < length:
            k1 = t * np.floor(sett["samplingRate"] / sett["overlap_sampling"])
            k2 = k1 + sett["lenAnal"]
            pointData = dataNormed[:, int(k1):int(k2)]
            dataTrain.append(pointData)
            features = calcFeatureSp(pointData, sett)
            featuresTrain.append(features)
            Ctrain.append(state)
            t = t + 1
            k = k + 1
    featuresTrainNp = np.asarray(featuresTrain)
    # featuresTrainNp [example=266, feature_index=264)
    fmean = np.mean(featuresTrainNp,0)
    fstd = np.std(featuresTrainNp,0)
    fmeanTile = np.tile(fmean, (featuresTrainNp.shape[0], 1))
    fstdTile =  np.tile(fstd, (featuresTrainNp.shape[0], 1))
    fnorm = (featuresTrainNp-fmeanTile)/fstdTile
    print "fnorm", fnorm.shape
    PCA = skd.PCA(n_components=sett["numPC"])
    fpca = PCA.fit_transform(fnorm)
    print "fpca", fpca.shape
    FLD = skda.LinearDiscriminantAnalysis(
        solver="svd", shrinkage = None, priors = None, n_components =
        None, store_covariance = False, tol = 0.0001)
    fFLD = FLD.fit_transform(fpca,Ctrain)
    print "fFLD", fFLD.shape
    # plt.figure()
    # plt.title("PCA")
    # plt.plot(fpca)
    #
    # plt.figure()
    # plt.title("FLD")
    # plt.scatter(fFLD, c=Ctrain)
    # plt.savefig("fFLD")
    tt=0
    resTrain = {"fmean":fmean, "fstd":fstd, "FLD":FLD, "PCA":PCA, "fFLD":fFLD, "Ctrain":Ctrain}
    print "trainind done"
    return resTrain

def test(dataEEG, resTrain, sett):
    dataMean = np.mean(dataEEG, 0)
    datarep = np.tile(dataMean, (dataEEG.shape[0], 1))
    dataNormed = dataEEG - datarep
    features = calcFeatureSp(dataNormed, sett)
    featuresNormed = (features-resTrain["fmean"])/resTrain["fstd"]
    featuresPCA = resTrain["PCA"].transform(featuresNormed)
    featuresFLD = resTrain["FLD"].transform(featuresPCA)
    fFLDtrain = resTrain["fFLD"]
    samplesCount = fFLDtrain.shape[0]
    allDists=np.zeros(samplesCount)
    for i in range(0,samplesCount):
        allDists[i]=euclidDist(fFLDtrain[i,:], featuresFLD)
    cTrain = resTrain["Ctrain"]
    indexes_sorted = np.argsort(allDists)
    cTrain = np.asarray(cTrain)
    cTrainSorted = cTrain[indexes_sorted]
    distsSorted = allDists[indexes_sorted]

    nearestPoins=cTrain[indexes_sorted[1:sett["kNear"]]]
    numClasses = len(np.unique(cTrain))
    cNear = np.zeros(numClasses)
    for i in range(0,numClasses):
       cNear[i]=np.sum(nearestPoins==i)
    resClass=np.argmax(cNear)+1


    #mean dist (point, neigh) less razrezennost of oblaka
    # m1 = cTrain==resClass
    # distMyClass = allDists[m1]
    # meanDist = np.mean(distMyClass)
    distMyClass = allDists[cTrain == resClass]
    bestPoints = np.argsort(distMyClass)[0:int(len(distMyClass)/3)]
    meanDist = np.mean(distMyClass[bestPoints])

    if (meanDist>sett["sigmaForAtefactNeutral"]):
        resClass==0
    return resClass

dataEEGpath1 = "E:\Dropbox\Diva code\Diva\output\Debug\pavel_neutral_concentrate_relax"
dataEEGpath2 = "E:\Dropbox\Diva code\Diva\output\Debug\pavel_neutral_concentrate_relax_2"
resTrainG=train(dataEEGpath1,settings)
f = pyedflib.EdfReader(dataEEGpath2 + "\\" + str(1)+".edf")
n = f.signals_in_file
signal_labels = f.getSignalLabels()
sigbufs = np.zeros((n, f.getNSamples()[0]))
for i in np.arange(n):
    sigbufs[i, :] = f.readSignal(i)
dataEEGtest = sigbufs[0:6,0:256]
res=test(dataEEGtest, resTrainG, settings)
print "state = ", res


# dataEEGtest = []
# dataEEGtrainAll = []
# for state in range(0, numState):
#     dataEEGchans=[]
#     for ch in range(0,numChan):
#         dataEEGchans.append(np.zeros(0, samplingRate, float))
#     dataEEGtrainAll.append(dataEEGchans)
# dataEEGtrainAll=np.asarray(dataEEGtrainAll)